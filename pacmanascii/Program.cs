﻿using System;

namespace pacmanascii
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string s in args) {
                switch (s) {
                    case "testproprietepublic":
                        TestProprietePublic tpp = new TestProprietePublic("Test Propriété Public");
                        tpp.Main(new string[0]);
                        break;
                    case "testproprieteprive":
                        TestProprietePublic tppr = new TestProprietePublic("Test Propriété Privé");
                        tppr.Main(new string[0]);
                        break;
                    case "testchampspublic":
                        TestChampsPublic tcp = new TestChampsPublic("Test Champs Public");
                        tcp.Main(new string[0]);
                        break;
                    case "testchampsprive":
                        TestChampsPrive tcpr = new TestChampsPrive("Test Champs Privé");
                        tcpr.Main(new string[0]);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
