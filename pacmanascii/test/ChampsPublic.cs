using System;

namespace pacmanascii {
    class TestChampsPublic : AbstractTest {
        public TestChampsPublic(string name) : base(name) {}
        public override bool test()
        {
            ChampsPublic cp = new ChampsPublic();
            cp.champspublic = 42;
            if (cp.champspublic == 42) {
                return true;
            } else {
                return false;
            }
        }
    }
    class ChampsPublic {
        public int champspublic;
    }
}