using System;

namespace pacmanascii {
    class TestProprietePublic : AbstractTest {
        public TestProprietePublic(string name) : base(name) {}
        public override bool test()
        {
            ProprietePublic cp = new ProprietePublic();
            cp.proprietepublic = 42;
            if (cp.proprietepublic == 42) {
                return true;
            } else {
                return false;
            }
        }
    }
    class ProprietePublic {
        public int proprietepublic {get; set;}
    }
}