using System;

namespace pacmanascii {
    class TestChampsPrive : AbstractTest {
        public TestChampsPrive(string name) : base(name) {}
        public override bool test()
        {
            // ChampsPrive cp = new ChampsPrive();
            // cp.champsprive = 42;
            // if (cp.champsprive == 42) {
            //     return true;
            // } else {
                return false; // Le champs ne peut être accessible ailleurs que dans la classe ChampsPrive car nous avons pas accès aux propriétés <- donc aux assesseurs
            // }
        }
    }
    class ChampsPrive {
        private int Champsprive;
    }
}