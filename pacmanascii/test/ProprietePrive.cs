using System;

namespace pacmanascii {
    class TestProprietePrive : AbstractTest {
        public TestProprietePrive(string name) : base(name) {}
        public override bool test()
        {
            ProprietePrive cp = new ProprietePrive();
            cp.proprieteprive = 42;
            if (cp.proprieteprive == 42) {
                return true;
            } else {
                return false;
            }
        }
    }
    class ProprietePrive {
        private int Proprieteprive;
        public int proprieteprive {get{return Proprieteprive;}set{Proprieteprive = value;}}
    }
}