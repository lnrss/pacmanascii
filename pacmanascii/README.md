# Pac-Man C# :video_game:
<i>Léo Nourrisson</i>

<b>3 - Make</b>
Question 1. C'est dans le fichier makefile que sont décrites : 
· les relations de dépendances entre programme (la cible) et modules, 
· les commandes (associées à une cible) à exécuter pour effectuer les mises à jour de la cible 
· Les « macro » qui facilitent l'écriture des relations et des commandes.
Alternative : scripts bash cependant, bash possède du multiples désavantages comme :
Les chemins: en relatif ou en absolu. Cela peut poser problème si tu déplaces ton script.
Tu vas tout recompiler. À moins de faire plus compliqué et de faire des tests d'existence.
<hr>
<b>4 - Héritage</b>
<p>Question 2. 
<pre>
class TestITest : ITest {
        public bool test(){
            return true;
        }
    }
</pre>
Question 4. L'intérêt de cette implémentation est de pouvoir faire tester les classes individuellement à l'aide du switch case ou faire un test global à l'aide de make qui exécutera chaque cas du switch afin de tester un par un. Cette architecture permet d'exécuter rapidement à l'aide d'une seule commande tous les dotnet run de chaque cas du switch, cacher les commandes afin d'ahérer le code de sortie</p>
<hr>
<b>5 - A vous de jouer !</b>
<p>Question 6. Le champs ne peut être accessible ailleurs que dans la classe ChampsPrive car nous avons pas accès aux propriétés <- donc aux assesseurs
<br>Question 13. On peut voir que cet environnement permet de voir l'intérêt des champs ainsi que des propriétés, à quoi servent les assesseurs ect. On peut aussi observer que cet environnement est automatisé par un script réalisé à l'aide du fichier makefile qui exécute les commandes une par une, pour faire simple le switch case demande une string en entrée pour chacun des tests, le fichier makefile va exécuter quatres commandes différentes qui, chacune d'elle auront pour effet de tester un cas du switch afin d'éviter de faire les quatres commandes manuellement. A savoir qu'on peut réaliser des tests solitaires. </p>